from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Skill(models.Model):
	skillID = models.IntegerField(null=False, primary_key=True)
	skillName = models.CharField(max_length=30, null=False)
	proficiency = models.IntegerField(null=False)

	def __str__(self):
		return self.skillName

class UserSkill(models.Model):
	userID = models.ForeignKey(to=User, on_delete=models.CASCADE)
	skillID = models.ForeignKey(to=Skill, on_delete=models.CASCADE)

	def __str__(self):
		return self.skillID

class Ticket(models.Model):
	SEVERITY = (
		('1', 'Critical'),
		('2', 'Major'),
		('3', 'Minor'),
		('4', 'Warning'),
		('5', 'Clear'),
	)
	STATUS = (
		('1', 'Open'),
		('2', 'In Progress'),
		('3', 'Done'),
		('4', 'Closed'),
	)
	VALIDITY = (
		('1', 'Valid'),
		('2', 'Invalid'),
		('3', 'Duplicate'),
	)
	id = models.AutoField(primary_key=True)
	title = models.CharField(max_length=50, null=True)
	description = models.TextField()
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(default=None, null=True, blank=True)
	closed_at = models.DateTimeField(default=None, null=True, blank=True)
	reportedBy = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name="reportedBy")
	assignedTo = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name="assignedTo", default=None, null=True, blank=True)
	status = models.CharField(max_length=2, choices=STATUS, default="1")
	severity = models.CharField(max_length=2, choices=SEVERITY, default="4")
	validity = models.CharField(max_length=2, choices=VALIDITY, default="1")


class Comments(models.Model):
	comment = models.TextField(null=True)
	creation_date = models.DateTimeField(auto_now_add=True, null=True, blank=True)
	ticket = models.ForeignKey(to=Ticket, on_delete=models.CASCADE)
	userID = models.ForeignKey(to=User, on_delete=models.CASCADE)
	
class Image(models.Model):
   image_path = models.CharField(max_length=255, null=True)
   comments = models.ForeignKey(to=Comments, on_delete=models.CASCADE)
