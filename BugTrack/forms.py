from django import forms
from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from BugTrack.models import Ticket


class CreateUserForm(UserCreationForm):
	class Meta:
		model = User
		fields = ['username', 'email', 'password1', 'password2']

class TicketForm(forms.ModelForm):
	class Meta:
		model = Ticket
		fields = ['title', 'severity', 'description']

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.helper = FormHelper()
		self.helper.form_id = 'id-ticketForm'
		self.helper.form_class = 'blueForms'
		self.helper.form_method = 'POST'
		self.helper.form_action = 'add_Ticket'
		self.helper.form_show_errors = False
		self.helper.error_text_inline = False

		self.helper.add_input(Submit('submit', 'Create'))

class editTicketsForm(ModelForm):
	class Meta:
		model = Ticket
		fields = ['title', 'description', 'assignedTo', 'status', 'severity', 'validity']

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.helper = FormHelper()
		self.helper.form_id = "id-editTicketForm"
		self.helper.form_class = "blueForms"
		self.helper.form_method = "POST"
		self.helper.form_action = ""
		self.helper.form_show_errors = False
		self.helper.error_text_inline = False

		self.helper.add_input(Submit('submit', 'Save'))

class CommentForm(forms.Form):
	content = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control","placeholder":"Add a public comment"}))	
