from django.conf.urls import url
from django.urls import path
from . import views

urlpatterns = [
	path('', views.loginView, name='login'),
	path('home/', views.indexView, name='home'),
	path('login/', views.loginView, name='login'),
	path('logout/', views.logoutView, name='logout'),
	path('forget_password', views.forgetPassword, name='forgetpassword'),
	path('change_password/<int:pk>', views.changePassword, name='changepassword'),	
	path('register/', views.registerView, name='register'),
	path('view_Ticket/', views.view_Ticket, name='viewticket'),
	path('myTicket/', views.myTicket, name='myTicket'),
	path('add_Ticket', views.add_Ticket, name='addticket'),
	path("ticket_Details/<int:pk>", views.ticket_details, name='ticketDetails'),
	path('filterView/', views.filterView, name='filterView'),
	path('viewProfile/',views.viewProfile, name='viewProfile'),
	path("editTickets/<int:pk>", views.editTickets, name='editTickets'),
	path("notAllowedToEdit/", views.notAllowedToEdit, name='notAllowedToEdit'),
]