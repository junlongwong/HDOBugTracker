import django_filters
from django_filters import CharFilter, DateFilter
from django.template.defaulttags import register
from django.forms.widgets import TextInput
from .models import *

class TicketFilter(django_filters.FilterSet):
	title = CharFilter(field_name='title', lookup_expr='icontains', label='Title', widget=TextInput(attrs={'style':'width:15ch'}))
	description = CharFilter(field_name='description', lookup_expr='icontains',label='Description', widget=TextInput(attrs={'style':'width:15ch'}))	
	creation_gte = DateFilter(field_name='created_at', lookup_expr='gte', label='Date created greater than or equal', widget=TextInput(attrs={'placeholder': 'mm/dd/yy', 'style':'width:12ch'}))
	creation_lte = DateFilter(field_name='created_at', lookup_expr='lte', label='Date created less than or equal', widget=TextInput(attrs={'placeholder': 'mm/dd/yy', 'style':'width: 12ch'}))
	ticket_id = CharFilter(field_name='id', lookup_expr='exact', widget=TextInput(attrs={'style':'width: 10ch'}))

	class Meta:
		model = Ticket
		fields ='__all__'
		exclude = ['created_at', 'updated_at','closed_at']
		

@register.filter
def get_item(dictionary, key):
	return dictionary.get(key)